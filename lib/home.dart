import 'package:flashy_tab_bar/flashy_tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:login/home_page.dart';
import 'package:login/register.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime selectedStartDate = DateTime.now();
  DateTime selectedEndDate = DateTime.now();
  String selectedFiler = 'This Week';
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final alucard = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/alucard.jpg'),
        ),
      ),
    );
    _selectStartDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedStartDate, // Refer step 1
        firstDate: DateTime(2000),
        lastDate: DateTime(2025),
      );
      if (picked != null) {
        setState(() {
          selectedStartDate = picked;
        });
      }
    }

    _selectEndDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate, // Refer step 1
        firstDate: DateTime(2000),
        lastDate: DateTime(2025),
      );
      if (picked != null) {
        setState(() {
          selectedEndDate = picked;
        });
      }
    }

    final filters = Container(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Row(
          children: [
            // Expanded(
            //     flex: 1,
            //     child: Container(
            //       child: DropdownButton<String>(
            //         items: <String>['This Week', 'This Month', 'This Year']
            //             .map((String value) {
            //           return DropdownMenuItem<String>(
            //             value: value,
            //             child: new Text(
            //               value,
            //               style: TextStyle(color: Colors.black),
            //             ),
            //           );
            //         }).toList(),
            //         value: selectedFiler,
            //         onChanged: (_) {},
            //       ),
            //     )),
            Expanded(
                flex: 1,
                child: Container(
                    child: Column(
                  children: [
                    Text('asdasd'),
                    GestureDetector(
                      child: Container(
                        child: Text(
                          "${selectedStartDate.toLocal()}".split(' ')[0],
                          style: TextStyle(
                              fontSize: 55, fontWeight: FontWeight.bold),
                        ),
                      ),
                      onTap: () {
                        _selectStartDate(context);
                      },
                    )
                  ],
                )))
          ],
        ),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[filters, alucard],
      ),
    );

    return Scaffold(
      body: body,
      bottomNavigationBar: FlashyTabBar(
        selectedIndex: _selectedIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          _selectedIndex = index;
        }),
        items: [
          FlashyTabBarItem(
            icon: Icon(Icons.event),
            title: Text('Events'),
          ),
          FlashyTabBarItem(
            icon: Icon(Icons.search),
            title: Text('Search'),
          ),
          FlashyTabBarItem(
            icon: Icon(Icons.highlight),
            title: Text('Highlights'),
          ),
          FlashyTabBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
          FlashyTabBarItem(
            icon: Icon(Icons.settings),
            title: Text('한국어'),
          ),
        ],
      ),
    );
  }
}
