import 'dart:convert';

import 'package:flashy_tab_bar/flashy_tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:login/home_page.dart';
import 'package:login/register.dart';
import 'package:login/services/apiService.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime selectedStartDate = DateTime.now();
  DateTime selectedEndDate = DateTime.now();
  String selectedFiler = 'This Week';

  final secureStorage = new FlutterSecureStorage();
  var _selectedIndex = 0;
  var carsView = [];
  var carUpdateId = '';
  bool isAddingCar = false;
  final emailC = TextEditingController();
  final passwprdC = TextEditingController();
  final nameC = TextEditingController();
  final addressC = TextEditingController();
  final mobileC = TextEditingController();
  final eIdC = TextEditingController();

  final lpC = TextEditingController();
  final modelC = TextEditingController();
  final typeC = TextEditingController();
  final colorC = TextEditingController();
  bool isProfileEditing = false;
  var carsBlock = [];
  var apiService = ApiService();
  refreshCars() {
    apiService.getUserCars().then((value) {
      carsView = [];
      for (var i = 0; i < value.length; i++) {
        var carData = {
          "lp": value[i]['car_no'],
          "model": value[i]['model_year'],
          "type": value[i]['car_type']
        };
        carsView.add(
            Container(
              child: Card(
                elevation: 50,
                shadowColor: Colors.black,
                color: Colors.greenAccent[100],
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.90,
                  height: 200,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [

                        Text(
                          'License: ' +  value[i]['car_no'],
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.green[900],
                          ), //Textstyle
                        ), //Text
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Model: ' +  value[i]['model_year'].toString(),
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.green[900],
                          ), //Textstyle
                        ), //Text
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Type: ' +  value[i]['car_type'],
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.green[900],
                          ), //Textstyle
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        GestureDetector(
                          child: Text(
                            'Update',
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.green[900],
                            ), //Textstyle
                          ),
                          onTap: () {
                            isAddingCar = true;
                            lpC.text = value[i]['car_no'];
                            modelC.text = value[i]['model_year'].toString();
                            typeC.text = value[i]['car_type'];
                            carUpdateId =  value[i]['id'].toString();
                            setState(() {

                            });
                          },
                        )


                      ],
                    ), //Column
                  ), //Padding
                ), //SizedBox
              ),
            )
        );
      }
      setState(() {

      });
    });
  }
  @override
  void initState() {
   refreshCars();

  }
  @override
  Widget build(BuildContext context) {
    final alucard = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/alucard.jpg'),
        ),
      ),
    );
    _selectStartDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedStartDate, // Refer step 1
        firstDate: DateTime(2000),
        lastDate: DateTime(2025),
      );
      if (picked != null) {
        setState(() {
          selectedStartDate = picked;
        });
      }
    }

    _selectEndDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate, // Refer step 1
        firstDate: DateTime(2000),
        lastDate: DateTime(2025),
      );
      if (picked != null) {
        setState(() {
          selectedEndDate = picked;
        });
      }
    }

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: emailC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final mobile = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: mobileC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Mobile no.',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () async{
          var payload = {
            "name": nameC.text,
            'emirates_id': eIdC.text,
            'mobile_no': mobileC.text,
            "address": addressC.text,
            "password": passwprdC.text
          };
          // var response = await apiService.register(payload);
          // if(response == 'true') {
          //   Navigator.of(context).pushNamed(HomePage.tag);
          // }
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Update', style: TextStyle(color: Colors.white)),
      ),
    );
    final address = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: addressC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Address',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final name = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: nameC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final eId = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: eIdC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Emirates ID',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwprdC,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final filters = Container(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Row(
          children: [
            Expanded(
                flex: 1,
                child: Container(
                  child: DropdownButton<String>(
                    items: <String>['This Week', 'This Month', 'This Year']
                        .map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: new Text(
                          value,
                          style: TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                    value: selectedFiler,
                    onChanged: (_) {},
                  ),
                )),
            Expanded(
                flex: 1,
                child: Container(
                    child: Column(
                      children: [
                        GestureDetector(
                          child: Container(
                            child: Text(
                              "Start: "+("${selectedStartDate.toLocal()}".split
                                (' ')
                              [0]),
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          onTap: () {
                            _selectStartDate(context);
                          },
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        GestureDetector(
                          child: Container(
                            child: Text(
                              "End: "+("${selectedEndDate.toLocal()}".split
                                (' ')
                              [0]),
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          onTap: () {
                            _selectEndDate(context);
                          },
                        )
                      ],
                    )))
          ],
        ),
      ),
    );
    final userDetials =  ListView(
      shrinkWrap: true,
      padding: EdgeInsets.only(left: 24.0, right: 24.0),
      children: <Widget>[
        eId,
        SizedBox(height: 10.0),
        email,
        SizedBox(height: 10.0),
        password,
        SizedBox(height: 10.0),
        name,
        SizedBox(height: 10.0),
        address,
        SizedBox(height: 10.0),
        mobile,
        SizedBox(height: 10.0),
        loginButton
      ],
    );
    final carAddLabel = Container(
      child: Text(
        'Add car details',
        style: TextStyle(color: Colors.black54, fontSize: 18),
      ),
    );
    final lp = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: lpC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'LP',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final model = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: modelC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Model (year)',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final type = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: typeC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Type',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final color = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: colorC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Color',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final saveCar = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () async{
          var darDetails = {
            "lp": lpC.text,
            "model": modelC.text,
            "type": typeC.text,
            "color": colorC.text,
          };
        print('------------'+carUpdateId);
          if(carUpdateId != '') {
            var payload = {
              "id": carUpdateId,
              "car_no": lpC.text,
              "model_year": modelC.text,
              "car_type": typeC.text,
              "user": jsonDecode(await secureStorage.read(key: 'user'))
              ['user_id']
            };
            await apiService.updateCar(payload);
            carUpdateId = '';
          } else {
            print(await secureStorage.read(key: 'user'));
            var payload = {
              "user": jsonDecode(await secureStorage.read(key: 'user'))
              ['user_id'],
              "car_no": lpC.text,
              "model_year": modelC.text,
              "car_type": typeC.text
            };
            await apiService.addCar(payload);
          }
          lpC.text = '';
          modelC.text = '';
          typeC.text = '';
          colorC.text = '';
         refreshCars();
          setState(() {
            isAddingCar = false;
            carUpdateId = '';
          });
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Save', style: TextStyle(color: Colors.white)),
      ),
    );

    final carAddForm = Container(
      child: isAddingCar ? Column(
        children: [
          carAddLabel,
          SizedBox(height: 24.0),
          lp,
          SizedBox(height: 24.0),
          model,
          SizedBox(height: 24.0),
          type,
          SizedBox(height: 24.0),
          color,
          SizedBox(height: 24.0),
          saveCar
        ],
      ) : Column(
        children: [
          Container(
            padding: EdgeInsets.all(50),
            alignment: Alignment.topRight,
            child: IconButton(
              icon: Icon(
                Icons.add,
              ),
              iconSize: 50,
              color: Colors.blue,
              splashColor: Colors.purple,
              onPressed: () {
                setState(() {
                  isAddingCar = true;
                });
              },
            ),
          ),
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              ...carsView
            ],
          ),
        ],
      ),
    );


    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[filters, alucard],
      ),
    );


    final profilePage = SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(28.0),
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            Colors.blue,
            Colors.lightBlueAccent,
          ]),
        ),
        child: Column(
          children: <Widget>[userDetials, carAddForm],
        ),
      ),
    );



    getActiveTab() {
      if(_selectedIndex == 0) {
        return body;
      } else if (_selectedIndex == 1) {
        return profilePage;
      }
    }
    return Scaffold(
      body: SafeArea(
        child: getActiveTab(),
      ),
      bottomNavigationBar: FlashyTabBar(
        selectedIndex: _selectedIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          _selectedIndex = index;
        }),
        items: [
          FlashyTabBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          FlashyTabBarItem(
            icon: Icon(Icons.verified_user),
            title: Text('Profile'),
          ),
          FlashyTabBarItem(
            icon: Icon(Icons.notifications),
            title: Text('News'),
          ),
        ],
      ),
    );
  }
}
