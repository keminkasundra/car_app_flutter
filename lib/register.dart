import 'package:flutter/material.dart';
import 'package:login/home_page.dart';
import 'package:login/login_page.dart';
import 'package:login/services/apiService.dart';

class RegisterPage extends StatefulWidget {
  static String tag = 'register-page';
  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final emailC = TextEditingController();
  final passwprdC = TextEditingController();
  final nameC = TextEditingController();
  final addressC = TextEditingController();
  final mobileC = TextEditingController();
  final eIdC = TextEditingController();
  final lpC = TextEditingController();
  final modelC = TextEditingController();
  final typeC = TextEditingController();
  final colorC = TextEditingController();
  var carsDetails = [];
  var carsView = [];
  bool isAddingCar = false;
  var apiService = ApiService();
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: emailC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final mobile = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: mobileC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Mobile no.',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final address = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: addressC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Address',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final name = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: nameC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final eId = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: eIdC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Emirates ID',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      controller: passwprdC,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () async{
          var payload = {
            'name': nameC.text,
            'emirates_id': eIdC.text,
            'mobile_no': mobileC.text,
            'email': emailC.text,
            'address': addressC.text,
            'password': passwprdC.text
          };
          var response = await apiService.register(payload);
          if(response != 'false') {
            for (var i = 0; i < carsDetails.length; i++) {
              var payload = {
                "user": response,
                "car_no": carsDetails[i]['lp'],
                "model_year": carsDetails[i]['model'],
                "car_type": carsDetails[i]['type']
              };
              var response2 = await apiService.addCar(payload);

            }
            Navigator.of(context).pushNamed(LoginPage.tag);
          }
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Register', style: TextStyle(color: Colors.white)),
      ),
    );
    final lp = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: lpC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'LP',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final model = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: modelC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Model (year)',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final type = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: typeC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Type',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final color = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: colorC,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Color',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final saveCar = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          var darDetails = {
            "lp": lpC.text,
            "model": modelC.text,
            "type": typeC.text,
            "color": colorC.text,
          };

          carsDetails.add(darDetails);
          carsView.add(
            Container(
              child: Card(
                elevation: 50,
                shadowColor: Colors.black,
                color: Colors.greenAccent[100],
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.90,
                  height: 200,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                    Text(
                      'LP: ' + lpC.text,
                      style: TextStyle(
                        fontSize: 30,
                        color: Colors.green[900],
                        fontWeight: FontWeight.w500,
                      ), //Textstyle
                    ), //Text
                    SizedBox(
                      height: 10,
                    ), //SizedBox
                    Text(
                     'Model: ' + modelC.text,
                      style: TextStyle(
                      fontSize: 15,
                      color: Colors.green[900],
                    ), //Textstyle
                  ), //Text
                  SizedBox(
                    height: 10,
                  ),
                    Text(
                      'Type: ' + typeC.text,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.green[900],
                      ), //Textstyle
                    ),
                  SizedBox(
                    height: 10,
                  ),
                    Text(
                      'Color: ' + colorC.text,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.green[900],
                      ), //Textstyle
                    ),
                      ],
                ), //Column
              ), //Padding
            ), //SizedBox
          ),
            )
          );
          lpC.text = '';
          modelC.text = '';
          typeC.text = '';
          colorC.text = '';
          setState(() {
            isAddingCar = false;
          });
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Save', style: TextStyle(color: Colors.white)),
      ),
    );
    final carAddLabel = Container(
      child: Text(
        'Add car details',
        style: TextStyle(color: Colors.black54, fontSize: 18),
      ),
    );
    final carAddForm = Container(
      child: isAddingCar ? Column(
        children: [
          carAddLabel,
          SizedBox(height: 24.0),
          lp,
          SizedBox(height: 24.0),
          model,
          SizedBox(height: 24.0),
          type,
          SizedBox(height: 24.0),
          color,
          SizedBox(height: 24.0),
          saveCar
        ],
      ) : Column(
        children: [
          Container(
            padding: EdgeInsets.all(50),
            alignment: Alignment.topRight,
            child: IconButton(
              icon: Icon(
                Icons.add,
              ),
              iconSize: 50,
              color: Colors.blue,
              splashColor: Colors.purple,
              onPressed: () {
                setState(() {
                  isAddingCar = true;
                });
              },
            ),
          ),
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              ...carsView
            ],
          ),
        ],
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Already have an account?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        Navigator.of(context).pushNamed(LoginPage.tag);
      },
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            eId,
            SizedBox(height: 24.0),
            email,
            SizedBox(height: 24.0),
            password,
            SizedBox(height: 24.0),
            name,
            SizedBox(height: 24.0),
            address,
            SizedBox(height: 24.0),
            mobile,
            SizedBox(height: 24.0),
            carAddForm,
            SizedBox(height: 24.0),
            loginButton,
            forgotLabel
          ],
        ),
      ),
    );
  }
}
