import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class ApiService  {
  String apiUrl = 'http://18.118.226.189/';
  final secureStorage = new FlutterSecureStorage();
 Future login(formFields) async {
    var token = '';
    var baseServiceUrl = apiUrl;
    String url = '${baseServiceUrl}apis/user/token/';
    var responseJson;
    String jsonString = jsonEncode(formFields);
    final response = await http.post(
      url,
      body: jsonString,
      headers: {
        'Content-Type': 'application/json',
      },
    );
    print("token" + response.body);
    responseJson = jsonDecode(response.body);
    print(responseJson);
    if (response.statusCode == 200) {
      secureStorage.write(
          key: "token", value: responseJson['token']);
      return "true";
    }
    return responseJson['message'];
  }

  Future register(formFields) async {
    var baseServiceUrl = apiUrl;
    String url = baseServiceUrl + "apis/user/add_user/";
    var responseJson;
    var token = '';
    String jsonString = jsonEncode(formFields);
    final response = await http.post(
      url,
      body: jsonString,
      headers: {
        'Content-Type': 'application/json',
      },
    );
    print("response.statusCode" + response.statusCode.toString());
    print("response.statusCode" + response.body.toString());
    if (response.statusCode == 201) {
      responseJson = jsonDecode(response.body);
      secureStorage.write(
          key: "user", value: jsonEncode(responseJson['data']));
      return responseJson['data']['user_id'];
    } else {
      responseJson = jsonDecode(response.body);
//      showTost(responseJson['error']);
      return "false";
    }
    return "false";
  }
  Future addCar(formFields) async {
    var baseServiceUrl = apiUrl;
    String url = baseServiceUrl + "apis/car/add_car_details";
    var responseJson;
    var token = '';
    String jsonString = jsonEncode(formFields);
    final response = await http.post(
      url,
      body: jsonString,
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      print('--------' + response.body.toString());
      return 'true';
//      return responseJson;
    } else {
      responseJson = jsonDecode(response.body);
//      showTost(responseJson['error']);
      return "false";
    }
    return "false";
  }
  Future updateCar(formFields) async {
    var baseServiceUrl = apiUrl;
    String url = baseServiceUrl + "apis/car/retrive_update_delete_car";
    var responseJson;
    var token = '';
    String jsonString = jsonEncode(formFields);
    final response = await http.put(
      url,
      body: jsonString,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + await secureStorage.read(key: "token")
      },
    );
    print("response.statusCode" + response.statusCode.toString());
    print("response.statusCode" + response.body.toString());
    if (response.statusCode == 200) {
      print('--------' + response.body.toString());
      return 'true';
//      return responseJson;
    } else {
      responseJson = jsonDecode(response.body);
//      showTost(responseJson['error']);
      return "false";
    }
    return "false";
  }
  Future getUserCars() async {
    var baseServiceUrl = apiUrl;
    String url = baseServiceUrl + "apis/car/retrive_update_delete_car";
    var responseJson;
    var token = '';
    final response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + await secureStorage.read(key: "token")
      },
    );
    print("response.statusCode" + response.statusCode.toString());
    print("response.statusCode" + response.body.toString());
    if (response.statusCode == 200) {
      print('--------' + response.body.toString());
      responseJson = jsonDecode(response.body);
      return responseJson['data'];
//      return responseJson;
    } else {
      responseJson = jsonDecode(response.body);
//      showTost(responseJson['error']);
      return "false";
    }
    return "false";
  }

}
